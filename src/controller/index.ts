import { ITask } from '../types'
import TaskCreator from './../components/task-creator'
import TaskList from './../components/task-list'

let ID = 4

class Controller {
  private data: ITask[]
  private taskCreator: TaskCreator
  private root: HTMLElement
  private taskList: TaskList
  constructor(data: ITask[]) {
    this.data = data
    this.taskCreator = new TaskCreator()
    this.taskList = new TaskList(this.data)
    this.root = document.querySelector('.content')

    this.root.addEventListener('add-task', (evt: CustomEvent) => {
      this.data.unshift({ done: false, value: evt.detail.value, id: ID++ })
      this.taskList.update(this.data)
    })

    this.root.addEventListener('task-delete', (evt: CustomEvent) => {
      let deletedTaskIndex = this.data.findIndex(
        (el) => el.id === evt.detail.id
      )
      this.data.splice(deletedTaskIndex, 1)
      this.taskList.update(this.data)
    })
  }

  show() {
    this.root.append(this.taskCreator, this.taskList)
  }
}

export default Controller
