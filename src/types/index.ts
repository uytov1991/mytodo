export interface ITask {
  done: boolean
  value: string
  id: number
}
