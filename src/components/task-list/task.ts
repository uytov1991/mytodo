import { ITask } from './../../types'

export default class Task extends HTMLElement {
  private $taskValue: HTMLElement
  private $taskCheckbox: HTMLInputElement
  private readonly value: string
  private readonly done: boolean
  private readonly _id: number
  constructor({ value, id, done }: ITask) {
    super()
    this.value = value
    this.done = done
    this._id = id
  }

  private get valueTask() {
    return this.$taskValue.textContent
  }

  private set valueTask(value: string) {
    this.$taskValue.textContent = value
  }

  private set doneTask(done: boolean) {
    this.$taskCheckbox.checked = done
  }

  private get doneTask() {
    return this.$taskCheckbox.checked
  }

  public get dataTask(): ITask {
    return {
      id: this._id,
      done: this.doneTask,
      value: this.valueTask,
    }
  }

  public update(task: ITask) {
    if (!task) {
      this.remove()
      return
    }
    if (task.value !== this.valueTask) {
      this.valueTask = task.value
    }
    if (task.done !== this.doneTask) {
      this.doneTask = task.done
    }
  }

  connectedCallback() {
    this.innerHTML = this.render()
    this.$taskCheckbox = this.querySelector('.task-checkbox')
    this.$taskValue = this.querySelector('.task-value')
  }

  private render() {
    return `
      <li class='task'>
        <label class='task-label'>
          <input class='task-checkbox visually-hidden' data-action='check' type='checkbox' ${
            this.done ? 'checked' : ''
          } />
          <svg class='task-marker'>
            <use href='#check-icon' />
          </svg>
          <span class='task-value'>${this.value}</span>
        </label>
        <button class='redact-snap snap' data-action='redact' >
          <svg>
            <use href='#edit-icon' />
          </svg>
        </button>
        <button class='delete-snap snap' data-action='delete' >
          <svg>
            <use href='#close-icon' />
          </svg>
        </button>
      </li>
    `
  }
}

customElements.define('task-elem', Task)
