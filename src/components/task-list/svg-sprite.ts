export default `
  <svg style="display: none">
    <symbol
    id="check-icon"
    viewBox="0 0 16 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M12.5714 4.57141L6.28573 10.8571L3.42859 7.99998"
        stroke="var(--check-icon-color, transparent)"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
    </symbol>

    <symbol id="edit-icon" width="17" height="17" viewBox="0 0 17 17" fill="none"    xmlns="http://www.w3.org/2000/svg">
  <g clip-path="url(#clip0)">
    <path d="M8.0257 3.61491H3.35903C3.00541 3.61491 2.66627 3.75539 2.41622 4.00544C2.16617 4.25549 2.0257 4.59463 2.0257 4.94825V14.2816C2.0257 14.6352 2.16617 14.9743 2.41622 15.2244C2.66627 15.4744 3.00541 15.6149 3.35903 15.6149H12.6924C13.046 15.6149 13.3851 15.4744 13.6352 15.2244C13.8852 14.9743 14.0257 14.6352 14.0257 14.2816V9.61491" stroke="#181818" style="stroke: var(--fill-style, #181818)" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M13.0257 2.61484C13.2909 2.34962 13.6506 2.20062 14.0257 2.20062C14.4008 2.20062 14.7605 2.34962 15.0257 2.61484C15.2909 2.88005 15.4399 3.23976 15.4399 3.61484C15.4399 3.98991 15.2909 4.34962 15.0257 4.61484L8.69236 10.9482L6.0257 11.6148L6.69236 8.94817L13.0257 2.61484Z" stroke="#181818" style="stroke: var(--fill-style, #181818)" stroke-linecap="round" stroke-linejoin="round"/>
  </g>
  <defs>
    <clipPath id="clip0">
      <rect width="16" height="16" fill="white" transform="translate(0.692383 0.948242)"/>
    </clipPath>
  </defs>
</symbol>

<symbol id='close-icon' width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M14.25 4.8075L13.1925 3.75L9 7.9425L4.8075 3.75L3.75 4.8075L7.9425 9L3.75 13.1925L4.8075 14.25L9 10.0575L13.1925 14.25L14.25 13.1925L10.0575 9L14.25 4.8075Z" style="fill: var(--fill-style,  #181818)"/>
</symbol>
  </svg>
`
