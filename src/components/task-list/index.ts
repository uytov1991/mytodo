import { ITask } from '../../types'
import css from './style.scss'
import svgSprite from './svg-sprite'
import Task from './task'

type TaskActions = 'delete' | 'check' | 'redact'

export default class TaskList extends HTMLElement {
  private $list: HTMLElement
  private tasks: Task[]
  constructor(tasks: ITask[]) {
    super()
    this.attachShadow({ mode: 'open' })
    this.tasks = tasks.map((el) => new Task(el))
  }

  connectedCallback() {
    this.shadowRoot.innerHTML = this.render()
    this.$list = this.shadowRoot.querySelector('.list')
    this.$list.append(...this.tasks)
    this.$list.addEventListener('click', (evt) => {
      let target = evt.target as HTMLElement
      let btn = target.closest('[data-action]') as HTMLElement
      if (!btn) return

      let action = btn.dataset.action as TaskActions
      if (action) {
        let task = target.closest('task-elem') as Task
        this[action](task)
      }
    })
  }

  private redact(task: Task) {
    const { id, value } = task.dataTask
    this.dispatchEvent(
      new CustomEvent('task-redact', {
        bubbles: true,
        composed: true,
        detail: { id, value },
      })
    )
  }

  private check(task: Task) {
    const { id, done } = task.dataTask
    this.dispatchEvent(
      new CustomEvent('task-check', {
        bubbles: true,
        composed: true,
        detail: { id, done },
      })
    )
  }

  private delete(task: Task) {
    const { id } = task.dataTask
    this.dispatchEvent(
      new CustomEvent('task-delete', {
        bubbles: true,
        composed: true,
        detail: { id },
      })
    )
  }

  private render() {
    return `
      <style>${css}</style>
      ${svgSprite}
      <ul class='list'>
      </ul>
    `
  }

  private addTask(task: ITask) {
    this.$list.prepend(new Task(task))
  }

  public update(tasks: ITask[]) {
    let tasksElement = Array.from(this.$list.children)
    if (tasks.length > this.$list.children.length) {
      for (let i = 0; i < tasks.length; i++) {
        let task = tasksElement.find(
          (el: Task) => el.dataTask.id === tasks[i].id
        )
        if (!task) {
          this.addTask(tasks[i])
        }
      }
    } else {
      tasksElement.forEach((el: Task) => {
        let taskFromState = tasks.find((obj) => obj.id === el.dataTask.id)
        el.update(taskFromState)
      })
    }
  }
}

customElements.define('task-list', TaskList)
