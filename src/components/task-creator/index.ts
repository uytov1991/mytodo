import css from './styles.scss'
import svgSprite from './svg-sprite'

export default class TaskCreator extends HTMLElement {
  private $input: HTMLInputElement | null
  private $addSnap: HTMLButtonElement | null

  constructor() {
    super()
    this.attachShadow({ mode: 'open' })
    this.addEventListener('keypress', (evt) => {
      if (evt.code === 'Enter' && this.$input.value.trim()) {
        this.dispatchEvent(
          new CustomEvent('add-task', {
            bubbles: true,
            composed: true,
            detail: {
              value: this.$input.value,
            },
          })
        )
        this.$input.value = ''
      }
    })
  }

  connectedCallback() {
    this.shadowRoot.innerHTML = this.render()
    this.$input = this.shadowRoot.querySelector('.input')
    this.$addSnap = this.shadowRoot.querySelector('.add-snap')
    this.$addSnap.addEventListener('click', (evt) => {
      if (this.$input.value.trim()) {
        this.dispatchEvent(
          new CustomEvent('add-task', {
            bubbles: true,
            composed: true,
            detail: {
              value: this.$input.value,
            },
          })
        )
        this.$input.value = ''
      }
    })
  }

  render(): string {
    return `
      <style>${css}</style>
      ${svgSprite}
      <input class='input' type='text' placeholder='Enter task' />
      <button class='add-snap'>
        <svg class='svg-snap'>
          <use href='#add-icon' width='18' height='18' />
        </svg>
      </button>
    `
  }
}

customElements.define('task-creator', TaskCreator)
