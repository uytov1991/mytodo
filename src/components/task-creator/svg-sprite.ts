const svgSprite: string = `
  <svg style='display: none' xmlns="http://www.w3.org/2000/svg">
    <symbol id='add-icon' viewBox="0 0 18 18" fill="none" >
      <path d="M14.25 9.75H9.75V14.25H8.25V9.75H3.75V8.25H8.25V3.75H9.75V8.25H14.25V9.75Z" fill="var(--add-icon-color,  #7d8e9d)"/>
    </symbol>
  </svg>
`
export default svgSprite
