import './styles/index.scss'

import Controller from './controller'

const data = [
  { id: 1, done: true, value: 'Привет' },
  { id: 2, done: false, value: 'МИР' },
  { id: 3, done: false, value: 'Hello' },
]

const cntr = new Controller(data)

cntr.show()
