const webpack = require('webpack')
const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const autoprefixer = require('autoprefixer')
const cssnano = require('cssnano')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = (env) => {
  const isProduction = env.production === true

  const cssLoaders = [
    {
      loader: 'css-loader',
      options: {
        sourceMap: !isProduction,
      },
    },
    {
      loader: 'postcss-loader',
      options: {
        postcssOptions: {
          plugins: [
            (() => {
              if (isProduction) {
                return autoprefixer(), cssnano()
              } else return autoprefixer()
            })(),
          ],
        },
        sourceMap: !isProduction,
      },
    },
    {
      loader: 'resolve-url-loader',
      options: {
        sourceMap: !isProduction,
      },
    },
    {
      loader: 'sass-loader',
      options: {
        sourceMap: true, // always true for work resolve-url-loader!!!
      },
    },
  ]

  return {
    mode: isProduction ? 'production' : 'development',
    entry: {
      index: './src/index.ts',
    },
    output: {
      path: path.join(__dirname, 'dist'),
      publicPath: './',
      filename: '[name].js',
    },

    optimization: {
      splitChunks: {
        chunks: 'all',
      },
    },

    devtool: isProduction ? 'nosources-source-map' : 'eval-cheap-source-map',

    devServer: {
      publicPath: '/',
      openPage: 'index.html',
      // hot: !isProduction
    },

    resolve: {
      extensions: ['.ts', '.tsx', '.js', 'css', '.scss'],
    },

    plugins: [
      new CleanWebpackPlugin(),
      new MiniCssExtractPlugin({
        filename: 'index.css',
      }),
      new CopyWebpackPlugin({
        patterns: [
          {
            from: './src/static',
            to: './static',
          },
        ],
      }),
      new HtmlWebpackPlugin({
        template: './src/pages/index.html',
        include: true,
      }),
    ],

    module: {
      rules: [
        {
          test: /\.ts$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                presets: ['@babel/preset-env', '@babel/preset-typescript'],
                plugins: [
                  '@babel/plugin-transform-runtime',
                  '@babel/plugin-proposal-class-properties',
                  '@babel/proposal-object-rest-spread',
                ],
              },
            },
          ],
        },
        {
          test: /\.scss$/,
          oneOf: [
            {
              issuer: /components/,
              use: cssLoaders,
            },
            {
              issuer: /index.ts/,
              use: [
                {
                  loader: MiniCssExtractPlugin.loader,
                },
                ...cssLoaders,
              ],
            },
          ],
        },
      ],
    },
  }
}
